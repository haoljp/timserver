package org.mayuanxiaonong.timserver.util;

/**
 * 字节处理工具类
 * 
 * @author 9527
 *
 */
public class ByteUtil {

	/**
	 * 将字节数组转换为int
	 * 
	 * @param bs
	 * @return
	 */
	public static int parseInt(byte[] bs) {
		int n = 0x0;
		for (int i = 0; i < 4 && i < bs.length; i++) {
			n = n << 8 | bs[i];
		}
		return n;
	}

}
