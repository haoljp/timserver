package org.mayuanxiaonong.timserver.channel;

import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

/**
 * Channel管理器
 * 
 * @author 9527
 *
 */
public class ChannelManager {

	private static Map<Integer, Channel> channels = new HashMap<Integer, Channel>();

	public static Channel getChannel(Integer key) {
		return channels.get(key);
	}

	public static Channel addChannel(Integer key, Channel channel) {
		return channels.put(key, channel);
	}

}
