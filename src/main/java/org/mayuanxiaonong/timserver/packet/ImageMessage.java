package org.mayuanxiaonong.timserver.packet;

import org.msgpack.annotation.Message;

/**
 * 图片消息
 * 
 * @author 9527
 *
 */
@Message
public class ImageMessage extends MessagePacket {

	private int format;
	private String thumb;
	private String original;

	public ImageMessage(int type) {
		super(type);
	}

	public ImageMessage(int type, String msgid, int chatType, int from,
			int to, int time, int sq, int msgType) {
		super(type, msgid, chatType, from, to, time, sq, msgType);
	}

	public ImageMessage(int type, String msgid, int chatType, int from,
			int to, int time, int sq, int msgType, int format,
			String thumb, String original) {
		super(type, msgid, chatType, from, to, time, sq, msgType);
		this.format = format;
		this.thumb = thumb;
		this.original = original;
	}

	public int getFormat() {
		return format;
	}

	public void setFormat(int format) {
		this.format = format;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	@Override
	public String toString() {
		return "ImageMessage [format=" + format + ", thumb=" + thumb
				+ ", original=" + original + ", toString()=" + super.toString()
				+ "]";
	}

}
