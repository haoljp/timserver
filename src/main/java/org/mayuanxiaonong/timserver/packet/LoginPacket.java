package org.mayuanxiaonong.timserver.packet;

import org.msgpack.annotation.Message;

/**
 * 登录报文，可序列化为MessagePack二进制报文
 * 
 * @author 9527
 *
 */
@Message
public class LoginPacket extends Packet {

	private int userid;
	private String token;

	public LoginPacket(int type) {
		super(type);
	}

	public LoginPacket(int type, int userid, String token) {
		super(type);
		this.userid = userid;
		this.token = token;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "LoginPacket [userid=" + userid + ", token=" + token
				+ ", getType()=" + getType() + "]";
	}

}
