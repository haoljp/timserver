package org.mayuanxiaonong.timserver.packet;

import org.msgpack.annotation.Message;

/**
 * 状态报文
 * 
 * @author 9527
 *
 */
@Message
public class PresencePacket extends Packet {

	private int userid;
	private int presence;

	public PresencePacket(int type) {
		super(type);
	}

	public PresencePacket(int type, int userid, byte presence) {
		super(type);
		this.userid = userid;
		this.presence = presence;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getPresence() {
		return presence;
	}

	public void setPresence(int presence) {
		this.presence = presence;
	}

	@Override
	public String toString() {
		return "PresencePacket [userid=" + userid + ", presence=" + presence
				+ ", getType()=" + getType() + "]";
	}

	public static class PRESENCE {
		public static final int ONLINE = 0;
		public static final int AWAY = 1;
		public static final int BUSY = 2;
		public static final int OFFLINE = 3;
	}

}
