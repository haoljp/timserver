package org.mayuanxiaonong.timserver.packet;

/**
 * 简单报文，心跳或其他指令报文，type为1个字节
 * 
 * @author 9527
 *
 */
public class SimplePacket extends Packet {

	public SimplePacket(int type) {
		super(type);
	}

}
