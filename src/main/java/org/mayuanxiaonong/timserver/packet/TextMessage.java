package org.mayuanxiaonong.timserver.packet;

import org.msgpack.annotation.Message;

/**
 * 文本消息，可序列化为MessagePack的二进制报文
 * 
 * @author 9527
 *
 */
@Message
public class TextMessage extends MessagePacket {

	private String msg;

	public TextMessage(int type) {
		super(type);
	}

	public TextMessage(int type, String msgid, int chatType, int from, int to,
			int time, int sq, int msgType) {
		super(type, msgid, chatType, from, to, time, sq, msgType);
	}

	public TextMessage(int type, String msgid, int chatType, int from, int to,
			int time, int sq, int msgType, String msg) {
		super(type, msgid, chatType, from, to, time, sq, msgType);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "TextMessage [msg=" + msg + ", toString()=" + super.toString()
				+ "]";
	}

}
