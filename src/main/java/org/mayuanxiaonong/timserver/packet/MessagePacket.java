package org.mayuanxiaonong.timserver.packet;

import org.msgpack.annotation.Message;

/**
 * 消息报文
 * 
 * @author 9527
 *
 */
@Message
public class MessagePacket extends Packet {

	private String msgid;
	private int chatType;
	private int from;
	private int to;
	private int time;
	private int sq;
	private int msgType;

	public MessagePacket(int type) {
		super(type);
	}

	public MessagePacket(int type, String msgid, int chatType, int from,
			int to, int time, int sq, int msgType) {
		super(type);
		this.msgid = msgid;
		this.chatType = chatType;
		this.from = from;
		this.to = to;
		this.time = time;
		this.sq = sq;
		this.msgType = msgType;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public int getChatType() {
		return chatType;
	}

	public void setChatType(int chatType) {
		this.chatType = chatType;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getSq() {
		return sq;
	}

	public void setSq(int sq) {
		this.sq = sq;
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

	@Override
	public String toString() {
		return "MessagePacket [msgid=" + msgid + ", chatType=" + chatType
				+ ", from=" + from + ", to=" + to + ", time=" + time + ", sq="
				+ sq + ", msgType=" + msgType + ", getType()=" + getType()
				+ "]";
	}
	
	public static class TYPE {
		public static final int TEXT = 1;
		public static final int IMAGE = 2;
		public static final int FILE = 3;
		public static final int VOICE = 4;
	}

}
