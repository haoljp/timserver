package org.mayuanxiaonong.timserver.packet;

import io.netty.channel.Channel;

/**
 * 后台逻辑处理的报文
 * 
 * @author 9527
 *
 */
public class Packet {

	/**
	 * 报文类型（1字节的简单报文{@link SimplePacket}，或2字节的基本报文
	 */
	private int type;
	private Channel channel;

	public Packet(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "Packet [type=" + type + "]";
	}

	public static class Type {
		public static final int PING = 0x10;
		public static final int PONG = 0x11;
		public static final int LOGIN = 0x1001;
		public static final int LOGOUT = 0x1002;
		public static final int MESSAGE = 0x1010;
		public static final int PRESENCE = 0x1011;
		public static final int BROADCAST = 0X1012;
	}

}
