package org.mayuanxiaonong.timserver.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

import org.mayuanxiaonong.timserver.packet.Packet;

/**
 * 报文体编码处理器，将{@link Packet}编码为{@link ByteBuf}
 * 
 * @author 9527
 *
 */
public class PacketEncoder extends MessageToMessageEncoder<Packet> {

	@Override
	protected void encode(ChannelHandlerContext ctx, Packet msg,
			List<Object> out) throws Exception {

	}

}
