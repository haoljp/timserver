package org.mayuanxiaonong.timserver.codec.msgpack;

import static org.mayuanxiaonong.timserver.pack.msgpack.IPack.HEAD_LENGTH_LEN;
import static org.mayuanxiaonong.timserver.pack.msgpack.IPack.HEAD_SIMPLE_TYPE_LEN;
import static org.mayuanxiaonong.timserver.pack.msgpack.IPack.HEAD_TYPE_LEN;
import static org.mayuanxiaonong.timserver.pack.msgpack.IPack.HEAD_VERSION_LEN;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

import org.mayuanxiaonong.timserver.codec.PacketDecoder;
import org.mayuanxiaonong.timserver.pack.msgpack.IPack;
import org.mayuanxiaonong.timserver.packet.ImageMessage;
import org.mayuanxiaonong.timserver.packet.LoginPacket;
import org.mayuanxiaonong.timserver.packet.MessagePacket;
import org.mayuanxiaonong.timserver.packet.Packet;
import org.mayuanxiaonong.timserver.packet.PresencePacket;
import org.mayuanxiaonong.timserver.packet.SimplePacket;
import org.mayuanxiaonong.timserver.packet.TextMessage;
import org.mayuanxiaonong.timserver.util.ByteUtil;
import org.msgpack.MessagePack;

/**
 * MessagePack报文体解码处理器
 * 
 * <pre>
 * 1、将{@link ByteBuf}解码为{@link IPack}
 * 
 * 2、将{@link IPack}构建为{@link Packet}
 * 
 * @author 9527
 *
 */
public class MessagePackDecoder extends PacketDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf msg,
			List<Object> out) throws Exception {
		super.decode(ctx, msg, out);

		if (msg.readableBytes() <= HEAD_LENGTH_LEN) {
			// 格式不正确
			return;
		}

		Packet packet = null;

		// 读取长度
		byte[] lengthBytes = this.readBytes(msg, HEAD_LENGTH_LEN);
		int length = ByteUtil.parseInt(lengthBytes);

		if (length != msg.readableBytes()
				|| (length != HEAD_SIMPLE_TYPE_LEN && length < HEAD_TYPE_LEN
						+ HEAD_VERSION_LEN)) {
			// 长度域不正确
		} else {
			if (length == HEAD_SIMPLE_TYPE_LEN) {
				// 读取业务类型
				byte[] typeBytes = this.readBytes(msg, HEAD_SIMPLE_TYPE_LEN);
				int type = ByteUtil.parseInt(typeBytes);
				packet = new SimplePacket(type);
			} else {
				// 读取业务类型
				byte[] typeBytes = this.readBytes(msg, HEAD_TYPE_LEN);
				int type = ByteUtil.parseInt(typeBytes);

//				// 读取报文版本号
//				byte[] versionBytes = this.readBytes(msg, HEAD_VERSION_LEN);
//				int version = ByteUtil.parseInt(versionBytes);
				
				// 读取报文体
				int packLength = length - HEAD_TYPE_LEN - HEAD_VERSION_LEN;
				byte[] packBytes = this.readBytes(msg, packLength);
				
				MessagePack pack = new MessagePack();
				
				switch (type) {
				case Packet.Type.LOGIN:
					packet = pack.read(packBytes, LoginPacket.class);
					break;
				case Packet.Type.PRESENCE:
					packet = pack.read(packBytes, PresencePacket.class);
					break;
				case Packet.Type.MESSAGE:
					MessagePacket msgPacket = pack.read(packBytes, MessagePacket.class);
					switch (msgPacket.getMsgType()) {
					case MessagePacket.TYPE.TEXT:
						packet = pack.read(packBytes, TextMessage.class);
						break;
					case MessagePacket.TYPE.IMAGE:
						packet = pack.read(packBytes, ImageMessage.class);
						break;
//					case MessagePacket.TYPE.FILE:
//						packet = pack.read(packBytes, .class);
//						break;
//					case MessagePacket.TYPE.VOICE:
//						packet = pack.read(packBytes, TextMessage.class);
//						break;
					default:
						break;
					}
				default:
					break;
				}

//				IPack iPack = MessagePackConfig.getInstance().getPack(type);
//				if (iPack == null) {
//					// 不支持的协议
//				} else if (version < iPack.getCompatibleVersion()) {
//					// 请求报文使用版本太低，不兼容服务端版本
//				} else {
//					int packLength = length - HEAD_TYPE_LEN - HEAD_VERSION_LEN;
//					byte[] packBytes = this.readBytes(msg, packLength);
//					IPack pack = new MessagePack().read(packBytes,
//							iPack.getClass());
//					packet = pack.toPacket();
//				}
			}
		}

		if (packet != null) {
			out.add(packet);
		}
	}

	private byte[] readBytes(ByteBuf msg, int n) {
		byte[] bs = new byte[n];
		msg.readBytes(bs);
		return bs;
	}

}
