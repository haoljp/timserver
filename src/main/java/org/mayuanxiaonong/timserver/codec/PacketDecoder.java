package org.mayuanxiaonong.timserver.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

import org.mayuanxiaonong.timserver.packet.Packet;

/**
 * 报文体解码处理器，将{@link ByteBuf}解码为{@link Packet}
 * 
 * @author 9527
 *
 */
public class PacketDecoder extends MessageToMessageDecoder<ByteBuf> {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf msg,
			List<Object> out) throws Exception {

	}

}
