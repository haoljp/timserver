package org.mayuanxiaonong.timserver.pojo;

public class Presence {

	private int uid;
	private String token;
	private int presence;
	private long time;
	private String host;
	private int port;
	private int vid;

	public Presence() {
		super();
	}

	public Presence(int uid, String token, int presence, long time,
			String host, int port, int vid) {
		super();
		this.uid = uid;
		this.token = token;
		this.presence = presence;
		this.time = time;
		this.host = host;
		this.port = port;
		this.vid = vid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getPresence() {
		return presence;
	}

	public void setPresence(int presence) {
		this.presence = presence;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getVid() {
		return vid;
	}

	public void setVid(int vid) {
		this.vid = vid;
	}

	@Override
	public String toString() {
		return "Presence [uid=" + uid + ", token=" + token + ", presence="
				+ presence + ", time=" + time + ", host=" + host + ", port="
				+ port + ", vid=" + vid + "]";
	}

}
