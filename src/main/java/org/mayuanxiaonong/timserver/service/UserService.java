package org.mayuanxiaonong.timserver.service;

public interface UserService {

	public boolean login(Integer uid, String token);

}
