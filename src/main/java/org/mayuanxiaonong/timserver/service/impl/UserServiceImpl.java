package org.mayuanxiaonong.timserver.service.impl;

import org.mayuanxiaonong.timserver.dao.UserDao;
import org.mayuanxiaonong.timserver.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDaoMongo;

	@Override
	public boolean login(Integer uid, String token) {
		return userDaoMongo.login(uid, token);
	}

}
