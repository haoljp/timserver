package org.mayuanxiaonong.timserver.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import org.mayuanxiaonong.timserver.packet.Packet;

/**
 * 登录处理器
 * 
 * @author 9527
 *
 */
public class LoginHandler extends SimpleChannelInboundHandler<Packet> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Packet msg)
			throws Exception {
		if (msg.getType() == Packet.Type.LOGIN) {
			// 登录处理
		} else {
			ctx.fireChannelRead(msg);
		}
	}

}
