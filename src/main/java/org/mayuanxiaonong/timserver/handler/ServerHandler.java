package org.mayuanxiaonong.timserver.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import org.mayuanxiaonong.timserver.handler.deliver.PacketDeliver;
import org.mayuanxiaonong.timserver.packet.Packet;

/**
 * 服务器主要处理器
 * 
 * @author 9527
 *
 */
public class ServerHandler extends SimpleChannelInboundHandler<Packet> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Packet packet)
			throws Exception {
		packet.setChannel(ctx.channel());
		PacketDeliver.getInstance().deliver(packet);
	}

}
