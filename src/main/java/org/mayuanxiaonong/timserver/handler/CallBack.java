package org.mayuanxiaonong.timserver.handler;

public interface CallBack {
	
	public void preHandle();
	
	public void postHandle();

}
