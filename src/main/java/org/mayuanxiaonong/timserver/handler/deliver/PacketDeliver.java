package org.mayuanxiaonong.timserver.handler.deliver;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.mayuanxiaonong.timserver.packet.Packet;

/**
 * 分发队列
 * 
 * @author 9527
 *
 */
public class PacketDeliver {

	private BlockingQueue<Packet> queue;
	private ThreadPoolExecutor executor;
	private PacketHandler handler;

	private static PacketDeliver instance = new PacketDeliver();

	public static PacketDeliver getInstance() {
		return instance;
	}

	private PacketDeliver() {
		this.queue = new LinkedBlockingQueue<Packet>();
		this.executor = new ThreadPoolExecutor(64, Integer.MAX_VALUE, 60L,
				TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		this.handler = new PacketHandler(queue);
	}

	public void deliver(Packet packet) throws InterruptedException {
		queue.put(packet);
		executor.execute(handler);
		System.out.println("active count = " + executor.getActiveCount());
	}

}
