package org.mayuanxiaonong.timserver.starter;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 服务器类加载器
 * 
 * @author 9527
 *
 */
public class ServerClassLoader extends URLClassLoader {

	public ServerClassLoader(ClassLoader parent, File confDir, File libDir)
			throws MalformedURLException {

		super(new URL[] { confDir.toURI().toURL(), libDir.toURI().toURL() },
				parent);

		File[] jars = libDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String smallName = name.toLowerCase();
				return smallName.endsWith(".jar") || smallName.endsWith(".zip");
			}
		});

		if (jars == null) {
			return;
		}

		for (File jar : jars) {
			if (jar.isFile()) {
				addURL(jar.toURI().toURL());
			}
		}
	}

}
