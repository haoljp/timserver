package org.mayuanxiaonong.timserver.starter;

import java.io.File;

import static org.mayuanxiaonong.timserver.config.SystemConfig.*;

/**
 * 服务器启动器
 * 
 * @author 9527
 *
 */
public class ServerStarter {

	public static void main(String[] args) {
		try {
			new ServerStarter().start();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Server start error");
			System.exit(1);
		}
	}

	private void start() throws Exception {
		ClassLoader parent = Thread.currentThread().getContextClassLoader();
		if (parent == null) {
			parent = this.getClass().getClassLoader();
			if (parent == null) {
				parent = ClassLoader.getSystemClassLoader();
			}
		}

		String baseDirString = System.getProperty("base.dir", DEFAULT_BASE_DIR);

		File confDir = new File(baseDirString, DEFAULT_CONF_DIR);
		if (!confDir.exists()) {
			throw new RuntimeException("Conf directory "
					+ confDir.getAbsolutePath() + " does not exist.");
		}

		File libDir = new File(baseDirString, DEFAULT_LIB_DIR);
		if (!libDir.exists()) {
			throw new RuntimeException("Lib directory "
					+ libDir.getAbsolutePath() + " does not exist.");
		}

		ClassLoader loader = new ServerClassLoader(parent, confDir, libDir);

		Thread.currentThread().setContextClassLoader(loader);

		Class<?> containerClass = loader
				.loadClass("org.mayuanxiaonong.timserver.Bootstrap");
		containerClass.newInstance();

	}

}
