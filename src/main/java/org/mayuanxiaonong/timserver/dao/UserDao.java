package org.mayuanxiaonong.timserver.dao;

public interface UserDao {

	public boolean login(Integer uid, String token);

}
