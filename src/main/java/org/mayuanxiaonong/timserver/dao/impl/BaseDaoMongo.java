package org.mayuanxiaonong.timserver.dao.impl;

import org.springframework.data.mongodb.core.MongoTemplate;

public class BaseDaoMongo {

	protected static final String USER_COLLECTION = "user";
	protected static final String PRESENCE_COLLECTION = "presence";

	private MongoTemplate mongoTemplate;

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

}
