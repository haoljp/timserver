package org.mayuanxiaonong.timserver.dao.impl;

import org.mayuanxiaonong.timserver.dao.UserDao;
import org.mayuanxiaonong.timserver.pojo.Presence;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class UserDaoMongo extends BaseDaoMongo implements UserDao {

	@Override
	public boolean login(Integer uid, String token) {
		Presence presence = getMongoTemplate()
				.findOne(
						new Query(Criteria.where("uid").is(uid).and("token")
								.is(token)), Presence.class,
						PRESENCE_COLLECTION);
		return presence != null;
	}

}
