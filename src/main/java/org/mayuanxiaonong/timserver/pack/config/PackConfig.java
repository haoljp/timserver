package org.mayuanxiaonong.timserver.pack.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 报文协议配置
 * 
 * @author 9527
 *
 * @param <K>
 *            操作类型
 * @param <V>
 *            对应的报文类
 */
public abstract class PackConfig<K, V> {

	private static final Logger log = LoggerFactory.getLogger(PackConfig.class);

	private final Map<K, V> PACKS = new HashMap<K, V>();

	public PackConfig() {
		load();
		print();
	}

	/**
	 * 加载报文协议配置
	 */
	protected abstract void load();

	public V getPack(K k) {
		return PACKS.get(k);
	}

	public void putPack(K k, V v) {
		PACKS.put(k, v);
	}

	/**
	 * 加载后打印配置
	 */
	protected void print() {
		for (Entry<K, V> entry : PACKS.entrySet()) {
			log.info("已加载报文协议: " + entry.getValue());
		}
	}

}
