package org.mayuanxiaonong.timserver.pack.msgpack;

import org.mayuanxiaonong.timserver.packet.Packet;
import org.msgpack.annotation.Ignore;

/**
 * MessagePack二进制协议报文头部分
 * 
 * @author 9527
 *
 */
public abstract class IPack {

	// 报文长度域位置
	public static final int HEAD_LENGTH_OFFSET = 0;
	// 报文头各字段长度
	public static final int HEAD_LENGTH_LEN = 4;
	public static final int HEAD_TYPE_LEN = 2;
	public static final int HEAD_SIMPLE_TYPE_LEN = 1;
	public static final int HEAD_VERSION_LEN = 1;

	@Ignore
	private int type;
	@Ignore
	private int version;
	@Ignore
	private int compatibleVersion;

	public IPack() {
		super();
	}

	public IPack(int type, int version, int compatibleVersion) {
		super();
		this.type = type;
		this.version = version;
		this.compatibleVersion = compatibleVersion;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getCompatibleVersion() {
		return compatibleVersion;
	}

	public void setCompatibleVersion(int compatibleVersion) {
		this.compatibleVersion = compatibleVersion;
	}

	public abstract IPack fromPacket(Packet packet);

	public abstract Packet toPacket();

}