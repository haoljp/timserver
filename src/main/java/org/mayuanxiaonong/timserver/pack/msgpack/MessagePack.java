package org.mayuanxiaonong.timserver.pack.msgpack;

import org.mayuanxiaonong.timserver.packet.Packet;
import org.msgpack.annotation.Message;

/**
 * MessagePack二进制协议报文体部分，即时消息
 * 
 * @author 9527
 *
 */
@Message
public class MessagePack extends IPack {

	public MessagePack() {
		this(Packet.Type.MESSAGE, 1, 1);
	}

	public MessagePack(int type, int version, int compatibleVersion) {
		super(type, version, compatibleVersion);
	}

	@Override
	public IPack fromPacket(Packet packet) {
		return null;
	}

	@Override
	public Packet toPacket() {
		return null;
	}

}
