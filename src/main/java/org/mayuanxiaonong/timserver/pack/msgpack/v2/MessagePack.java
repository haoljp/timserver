package org.mayuanxiaonong.timserver.pack.msgpack.v2;

import org.mayuanxiaonong.timserver.pack.msgpack.IPack;
import org.mayuanxiaonong.timserver.packet.Packet;
import org.msgpack.annotation.Message;

@Message
public class MessagePack extends IPack {

	public MessagePack() {
		this(Packet.Type.MESSAGE, 2, 1);
	}

	public MessagePack(int type, int version, int compatibleVersion) {
		super(type, version, compatibleVersion);
	}

	@Override
	public IPack fromPacket(Packet packet) {
		return null;
	}

	@Override
	public Packet toPacket() {
		return null;
	}

}
