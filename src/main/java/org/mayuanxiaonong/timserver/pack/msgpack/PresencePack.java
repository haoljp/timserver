package org.mayuanxiaonong.timserver.pack.msgpack;

import org.mayuanxiaonong.timserver.packet.Packet;
import org.msgpack.annotation.Message;

/**
 * MessagePack二进制协议报文体部分，状态
 * 
 * @author 9527
 *
 */
@Message
public class PresencePack extends IPack {

	public PresencePack() {
		this(Packet.Type.PRESENCE, 1, 1);
	}

	public PresencePack(int type, int version, int compatibleVersion) {
		super(type, version, compatibleVersion);
	}

	@Override
	public IPack fromPacket(Packet packet) {
		return null;
	}

	@Override
	public Packet toPacket() {
		return null;
	}

}
